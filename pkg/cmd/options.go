package options

import (
	"github.com/xanzy/go-gitlab"
	"k8s.io/cli-runtime/pkg/genericclioptions"
)

// GlobalOptions defines the plugin's accepted options
type GlobalOptions struct {
	Gitlab        *gitlab.Client
	GitlabToken   string
	GitlabBaseURL string
	Project       string
	Group         string
	Set           []string
	Instance      bool
	KubeFlags     *genericclioptions.ConfigFlags
}
