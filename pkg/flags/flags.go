package flags

import (
	"fmt"
	"strings"

	"github.com/mitchellh/mapstructure"
)

// DecodeSetFlags decodes a slice of strings {"a.b.c=x", "a.b.d=y", "e=z"} into a nested map structure
// { "a": { "b": { "c": "x", "d": "y" }, "e": "z" }}
func DecodeSetFlags(flags []string) (result map[string]interface{}, err error) {
	result = make(map[string]interface{})

	for _, f := range flags {

		err = decodeFlag(&result, f)
		if err != nil {
			return
		}
	}

	return
}

func decodeFlag(dst *map[string]interface{}, flag string) error {
	firstDotIndex := strings.Index(flag, ".")
	firstEqualsIndex := strings.Index(flag, "=")

	if firstEqualsIndex == -1 {
		return fmt.Errorf("Set flag must have an assignment")
	}

	// Found the deepest nested key
	if firstDotIndex == -1 || firstDotIndex > firstEqualsIndex {
		k := flag[0:firstEqualsIndex]
		v := flag[firstEqualsIndex+1:]

		(*dst)[k] = v

		return nil
	}

	// Initializing a new map for the next nested map for the key
	k := flag[0:firstDotIndex]
	if _, ok := (*dst)[k]; !ok {
		(*dst)[k] = map[string]interface{}{}
	}

	// nextDst keeps track of all the flags that have been parsed during the outer for loop
	nextDst, ok := (*dst)[k].(map[string]interface{})
	if !ok {
		// happens if --set a.b=c and --set a.b.c = d are both present
		return fmt.Errorf("cannot decode into a key that already has a concrete value")
	}

	// nextFlag specifies the next flag to be parsed in this for loop iteration
	nextFlag := flag[firstDotIndex+1:]

	decodeFlag(&nextDst, nextFlag)

	return nil
}

// MergeOverridesMap merges a nested map of strings (src) into the destination structure (dst) with a predefined config
func MergeOverridesMap(dst interface{}, src map[string]interface{}) error {
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		TagName:          "json",
		WeaklyTypedInput: true,
		ZeroFields:       false,
		ErrorUnused:      true,
		Result:           dst,
	})

	if err != nil {
		return err
	}

	return decoder.Decode(src)
}
