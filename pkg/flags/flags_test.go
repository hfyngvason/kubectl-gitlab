package flags

import (
	"reflect"
	"strings"
	"testing"
)

func TestDecodeSetFlags(t *testing.T) {
	flags := []string{"a.b.c=x", "a.b.d=y", "e=z"}

	expectedResult := map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{"c": "x", "d": "y"},
		},
		"e": "z",
	}

	result, err := DecodeSetFlags(flags)
	if err != nil {
		t.Error(err)
		return
	}
	if !reflect.DeepEqual(result, expectedResult) {
		t.Errorf("expected %v but got %v", expectedResult, result)
		return
	}
}

func TestDecodeFlagHappyPath(t *testing.T) {
	dst := map[string]interface{}{"cat": "orange"}
	flag := "horse.fur=blue"

	expectedDst := map[string]interface{}{
		"cat":   "orange",
		"horse": map[string]interface{}{"fur": "blue"},
	}

	err := decodeFlag(&dst, flag)

	if err != nil {
		t.Error(err)
		return
	}
	if !reflect.DeepEqual(dst, expectedDst) {
		t.Errorf("expected %v but got %v", expectedDst, dst)
		return
	}

	// test writing into an existing nested map
	flag = "horse.nose=big"
	expectedDst = map[string]interface{}{
		"cat":   "orange",
		"horse": map[string]interface{}{"fur": "blue", "nose": "big"},
	}
}

func TestDecodeFlagErrorMissingAssignment(t *testing.T) {
	dst := map[string]interface{}{}
	flag := "horse"

	err := decodeFlag(&dst, flag)

	expectedErrorSubstr := "flag must have an assignment"
	if err == nil {
		t.Errorf("expected an error containing %v, but succeeded", expectedErrorSubstr)
		return
	}
	if !strings.Contains(err.Error(), expectedErrorSubstr) {
		t.Errorf("expected an error containing %v, but got %v", expectedErrorSubstr, err)
	}
}

func TestDecodeFlagErrorOverwriteValueType(t *testing.T) {
	dst := map[string]interface{}{"cat": "orange"}
	flag := "cat.orange=hello"

	expectedErrorSubstr := "already has a concrete value"
	err := decodeFlag(&dst, flag)
	if err == nil {
		t.Errorf("expected an error containing %v, but succeeded", expectedErrorSubstr)
		return
	}
	if !strings.Contains(err.Error(), expectedErrorSubstr) {
		t.Errorf("expected an error containing %v, but got %v", expectedErrorSubstr, err)
	}
}
