package entities

import (
	"fmt"

	"github.com/xanzy/go-gitlab"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
)

// Cluster is a helper struct to simplify serialization of GitLab API responses
type Cluster struct {
	ID               int    `json:"id"`
	Name             string `json:"name"`
	EnvironmentScope string `json:"environment_scope"`
	ClusterType      string `json:"cluster_type"`
	URL              string `json:"url"`
}

// FromProjectCluster creates a cluster from a project cluster
func FromProjectCluster(o *options.GlobalOptions, cluster *gitlab.ProjectCluster) Cluster {
	var url string
	if cluster.Project != nil {
		url = fmt.Sprintf("%s/-/clusters/%d", cluster.Project.WebURL, cluster.ID)
	}
	return Cluster{
		ID:               cluster.ID,
		Name:             cluster.Name,
		EnvironmentScope: cluster.EnvironmentScope,
		ClusterType:      "project",
		URL:              url,
	}
}

// FromGroupCluster creates a cluster from a project cluster
func FromGroupCluster(o *options.GlobalOptions, cluster *gitlab.GroupCluster) Cluster {
	var url string
	if cluster.Group != nil {
		url = fmt.Sprintf("%s/-/clusters/%d", cluster.Group.WebURL, cluster.ID)
	}
	return Cluster{
		ID:               cluster.ID,
		Name:             cluster.Name,
		EnvironmentScope: cluster.EnvironmentScope,
		ClusterType:      "group",
		URL:              url,
	}
}

// FromInstanceCluster creates a cluster from a project cluster
func FromInstanceCluster(o *options.GlobalOptions, cluster *gitlab.InstanceCluster) Cluster {
	return Cluster{
		ID:               cluster.ID,
		Name:             cluster.Name,
		EnvironmentScope: cluster.EnvironmentScope,
		ClusterType:      "instance",
		URL:              fmt.Sprintf("%s/admin/clusters/%d", o.GitlabBaseURL, cluster.ID),
	}
}
