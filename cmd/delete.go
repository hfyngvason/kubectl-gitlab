package cmd

import (
	"strconv"

	"github.com/spf13/cobra"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
)

// NewDelCmd initializes a new "del" subcommand. It expects the GlobalOptions param to be initialized
func NewDelCmd(o *options.GlobalOptions) *cobra.Command {
	delCmd := &cobra.Command{
		Use:     "delete [CLUSTER_ID] [flags]",
		Aliases: []string{"del"},
		Short:   "Delete a project, group or instance cluster",
		Args:    cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			clusterID, err := strconv.Atoi(args[0])
			if err != nil {
				// TODO: Allow deleting cluster by name
				return err
			}
			if o.Project != "" {
				_, err = o.Gitlab.ProjectCluster.DeleteCluster(o.Project, clusterID)
				if err != nil {
					return err
				}
			} else if o.Group != "" {
				_, err = o.Gitlab.GroupCluster.DeleteCluster(o.Group, clusterID)
				if err != nil {
					return err
				}
			} else if o.Instance {
				_, err = o.Gitlab.InstanceCluster.DeleteCluster(clusterID)
				if err != nil {
					return err
				}
			}
			return nil
		},
	}
	o.KubeFlags.AddFlags(delCmd.Flags())
	return delCmd
}
