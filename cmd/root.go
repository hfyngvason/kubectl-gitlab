package cmd

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
)

// NewRootCmd initializes a new root command and the given GlobalOptions
func NewRootCmd(o *options.GlobalOptions) *cobra.Command {
	rootCmd := &cobra.Command{
		Use:   "gitlab [command] [flags]",
		Short: "Manage GitLab cluster associations",
	}

	rootCmd.PersistentFlags().String("gitlab-url", o.GitlabBaseURL, "URL to your GitLab instance")
	rootCmd.PersistentFlags().String("gitlab-token", "", "GitLab API token")
	rootCmd.PersistentFlags().StringP("project", "p", "", "GitLab project")
	rootCmd.PersistentFlags().StringP("group", "g", "", "GitLab group")
	rootCmd.PersistentFlags().BoolP("instance", "i", false, "Instance wide")

	rootCmd.PersistentPreRunE = func(cmd *cobra.Command, args []string) error {
		var err error

		o.GitlabToken, err = cmd.Flags().GetString("gitlab-token")
		if err != nil {
			return err
		}
		if o.GitlabToken == "" {
			o.GitlabToken = os.Getenv("GITLAB_TOKEN")
		}
		if o.GitlabToken == "" {
			return fmt.Errorf("A GitLab API token is required. Use the flag --gitlab-token or an environment variable GITLAB_TOKEN")
		}

		o.GitlabBaseURL, err = cmd.Flags().GetString("gitlab-url")
		if err != nil {
			return err
		}
		if o.GitlabBaseURL == "" {
			o.GitlabBaseURL = os.Getenv("GITLAB_URL")
		}
		if o.GitlabBaseURL == "" {
			o.GitlabBaseURL = "https://gitlab.com"
		}

		o.Gitlab, err = gitlab.NewClient(o.GitlabToken, gitlab.WithBaseURL(o.GitlabBaseURL+"/api/v4"))
		if err != nil {
			return errors.Wrap(err, "failed to initialize the GitLab client")
		}

		o.Project, err = cmd.Flags().GetString("project")
		if err != nil {
			return err
		}

		o.Group, err = cmd.Flags().GetString("group")
		if err != nil {
			return err
		}

		o.Instance, err = cmd.Flags().GetBool("instance")
		if err != nil {
			return err
		}

		return validateClusterOptions(o)
	}

	return rootCmd
}

func validateClusterOptions(o *options.GlobalOptions) error {
	count := 0
	if o.Instance {
		count++
	}
	if o.Group != "" {
		count++
	}
	if o.Project != "" {
		count++
	}
	if count != 1 {
		return fmt.Errorf("Exactly one of --project, --group and --instance should be used")
	}
	return nil
}
