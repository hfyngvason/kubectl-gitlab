package cmd

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	entities "gitlab.com/hfyngvason/kubectl-gitlab/pkg"
	options "gitlab.com/hfyngvason/kubectl-gitlab/pkg/cmd"
	"gitlab.com/hfyngvason/kubectl-gitlab/pkg/flags"
	v1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth" // required for e.g. GCP, see https://github.com/kubernetes/client-go/issues/242
)

// NewAddCmd returns a new "add" subcommand. It expects the GlobalOptions param to be initialized
func NewAddCmd(o *options.GlobalOptions) *cobra.Command {
	addCmd := &cobra.Command{
		Use:   "add [flags]",
		Short: "Add cluster to GitLab",
		RunE: func(cmd *cobra.Command, args []string) error {
			ctx := context.Background()
			namespace := "kube-system"
			serviceAccountName := "gitlab"
			clusterRoleBindingName := "gitlab-admin"

			environmentScope, err := cmd.Flags().GetString("scope")
			if err != nil {
				return err
			}
			gitlabPayloadOverrides, err := flags.DecodeSetFlags(o.Set)
			if err != nil {
				return fmt.Errorf("failed to decode flags: %v", err)
			}

			loader := o.KubeFlags.ToRawKubeConfigLoader()

			// get cluster name
			rawConfig, err := loader.RawConfig()
			if err != nil {
				return fmt.Errorf("failed to load kubeconfig: %v", err)
			}
			var gitlabClusterName string
			if *o.KubeFlags.ClusterName != "" {
				gitlabClusterName = *o.KubeFlags.ClusterName
			} else {
				currentContext, ok := rawConfig.Contexts[rawConfig.CurrentContext]
				if !ok {
					return fmt.Errorf("unable to find config for context \"%v\"", rawConfig.CurrentContext)
				}
				gitlabClusterName = currentContext.Cluster
			}

			// init Kubernetes Clientset
			clientConfig, err := loader.ClientConfig()
			if err != nil {
				return fmt.Errorf("failed to initialized kubernetes client config: %v", err)
			}
			clientset, err := kubernetes.NewForConfig(clientConfig)
			if err != nil {
				return err
			}

			// find or create k8s resources
			gitlabAdminAccount := &v1.ServiceAccount{
				ObjectMeta: metav1.ObjectMeta{
					Name:      serviceAccountName,
					Namespace: namespace,
				},
			}
			_, err = clientset.CoreV1().ServiceAccounts(namespace).Create(
				ctx,
				gitlabAdminAccount,
				metav1.CreateOptions{},
			)
			if err != nil && !errors.IsAlreadyExists(err) {
				return err
			}

			gitlabAdminAccountSecret := &v1.Secret{
				ObjectMeta: metav1.ObjectMeta{
					Name:      serviceAccountName,
					Namespace: namespace,
					Annotations: map[string]string{
						"kubernetes.io/service-account.name": serviceAccountName,
					},
				},
				Type: "kubernetes.io/service-account-token",
			}
			_, err = clientset.CoreV1().Secrets(namespace).Create(
				ctx,
				gitlabAdminAccountSecret,
				metav1.CreateOptions{},
			)
			if err != nil && !errors.IsAlreadyExists(err) {
				return err
			}

			gitlabAdminClusterRoleBinding := &rbacv1.ClusterRoleBinding{
				ObjectMeta: metav1.ObjectMeta{
					Name: clusterRoleBindingName,
				},
				RoleRef: rbacv1.RoleRef{
					APIGroup: "rbac.authorization.k8s.io",
					Kind:     "ClusterRole",
					Name:     "cluster-admin",
				},
				Subjects: []rbacv1.Subject{
					{
						Kind:      "ServiceAccount",
						Name:      serviceAccountName,
						Namespace: namespace,
					},
				},
			}
			_, err = clientset.RbacV1().ClusterRoleBindings().Create(
				ctx,
				gitlabAdminClusterRoleBinding,
				metav1.CreateOptions{},
			)
			if err != nil && !errors.IsAlreadyExists(err) {
				return err
			}

			secret, err := clientset.CoreV1().Secrets(namespace).Get(ctx, serviceAccountName, metav1.GetOptions{})
			if err != nil {
				return err
			}

			// add to GitLab
			token := string(secret.Data["token"])
			cert := string(secret.Data["ca.crt"])
			apiURL := clientConfig.Host + clientConfig.APIPath

			if o.Project != "" {
				addClusterOptions := &gitlab.AddClusterOptions{
					Name:             &gitlabClusterName,
					EnvironmentScope: &environmentScope,
					PlatformKubernetes: &gitlab.AddPlatformKubernetesOptions{
						APIURL: &apiURL,
						Token:  &token,
						CaCert: &cert,
					},
				}
				err = flags.MergeOverridesMap(addClusterOptions, gitlabPayloadOverrides)
				if err != nil {
					return err
				}

				gitlabCluster, _, err := o.Gitlab.ProjectCluster.AddCluster(
					o.Project,
					addClusterOptions,
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromProjectCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else if o.Group != "" {
				addGroupClusterOptions := &gitlab.AddGroupClusterOptions{
					Name:             &gitlabClusterName,
					EnvironmentScope: &environmentScope,
					PlatformKubernetes: &gitlab.AddGroupPlatformKubernetesOptions{
						APIURL: &apiURL,
						Token:  &token,
						CaCert: &cert,
					},
				}
				err = flags.MergeOverridesMap(addGroupClusterOptions, gitlabPayloadOverrides)
				if err != nil {
					return err
				}
				gitlabCluster, _, err := o.Gitlab.GroupCluster.AddCluster(
					o.Group,
					addGroupClusterOptions,
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromGroupCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else if o.Instance {
				addInstanceClusterOptions := &gitlab.AddClusterOptions{
					Name:             &gitlabClusterName,
					EnvironmentScope: &environmentScope,
					PlatformKubernetes: &gitlab.AddPlatformKubernetesOptions{
						APIURL: &apiURL,
						Token:  &token,
						CaCert: &cert,
					},
				}
				err = flags.MergeOverridesMap(addInstanceClusterOptions, gitlabPayloadOverrides)
				if err != nil {
					return err
				}
				gitlabCluster, _, err := o.Gitlab.InstanceCluster.AddCluster(
					addInstanceClusterOptions,
				)
				if err != nil {
					return err
				}
				clusterEntity := entities.FromInstanceCluster(o, gitlabCluster)
				result, err := json.MarshalIndent(clusterEntity, "", "    ")
				if err != nil {
					return err
				}
				fmt.Println(string(result))
			} else {
				return fmt.Errorf("invalid cluster kind")
			}

			return nil
		},
	}
	o.KubeFlags.AddFlags(addCmd.Flags())
	addCmd.Flags().String("scope", "*", "GitLab environment scope for the cluster")
	addCmd.Flags().StringArrayVar(&o.Set, "set", []string{}, "Override any GitLab API JSON body variable, for example platform_kubernetes_attributes.api_url=https://example.com, see the API docs https://docs.gitlab.com/ee/api/project_clusters.html#add-existing-cluster-to-project")
	return addCmd
}
